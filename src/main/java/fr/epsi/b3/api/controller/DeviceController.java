package fr.epsi.b3.api.controller;

import fr.epsi.b3.api.dao.ValeurException;
import fr.epsi.b3.api.modele.Capteur;
import fr.epsi.b3.api.modele.Device;
import fr.epsi.b3.api.modele.Valeur;
import fr.epsi.b3.api.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private CapteurService capteurService;

    @Autowired
    private ValeurService valeurService;

    @ExceptionHandler(DeviceException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public String handleExistingDevice(DeviceException e) {
        return e.getMessage();
    }

    @ExceptionHandler(CapteurException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public String handleExistingDevice(CapteurException e) {
        return e.getMessage();
    }

    @ExceptionHandler(ValeurException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public String handleExistingDevice(ValeurException e) {
        return e.getMessage();
    }

    @GetMapping(path = "/devices", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Device>> getAllDevices() {
        return ResponseEntity.ok().body(deviceService.getAllDevices());
    }

    @PostMapping(path = "/devices", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Device>> insertDevice(@RequestBody Device device) throws DeviceException, CapteurException, ValeurException {
        deviceService.insertDevice(device);
        return ResponseEntity.ok().body(deviceService.getAllDevices());
    }

    @PostMapping(path = "devices/{device_id}/{type_capteur}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Device> insertValuesCapteur(@PathVariable String device_id, @PathVariable String type_capteur, @RequestBody Valeur valeur) throws DeviceException, CapteurException, ValeurException {
        deviceService.addValeurToCapteurAndUpdateDevice(deviceService.findDeviceByMacAdresse(device_id), type_capteur, valeur);
        return ResponseEntity.ok().body(deviceService.findDeviceByMacAdresse(device_id));
    }

}

