package fr.epsi.b3.api.controller;

import fr.epsi.b3.api.modele.Utilisateur;
import fr.epsi.b3.api.service.UserException;
import fr.epsi.b3.api.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;

@RestController
public class UtilisateurController {

    @Autowired
    private UtilisateurService utilisateurService;

    @ExceptionHandler(NoResultException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public String handleNotFoundUser(NoResultException e) {
        return e.getMessage();
    }

    @ExceptionHandler(UserException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public String handleBadIdentifers(UserException e) {
        return e.getMessage();
    }

    //Ajout d'un utilisateur dans la bdd
    @PostMapping(path = "/user/newuser", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Utilisateur> insertUtilisateurFromJson(@RequestBody Utilisateur utilisateur) throws UserException {
        utilisateurService.insertUtilisateur(utilisateur);
        return ResponseEntity.ok().body(utilisateur);
    }

    //Verification d'un user mot de passe
    @PostMapping(path = "/user", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Utilisateur> getUtilisateurByEmailAndPassword(@RequestBody Utilisateur utilisateur) throws UserException {
        return ResponseEntity.ok().body(utilisateurService.getUtilisateurByEmailAndPassword(utilisateur));
    }
}
