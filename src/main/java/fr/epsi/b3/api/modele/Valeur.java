package fr.epsi.b3.api.modele;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "valeurs")
@Table(name = "valeurs")
public class Valeur implements Serializable, Comparable<Valeur> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    @JsonIgnore
    private Long id;

    private double value;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date date_value;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "capteur_id")
    private Capteur capteur;

    @Override
    public int compareTo(Valeur v) {
        if (getDate_value() == null || v.getDate_value() == null) {
            return 0;
        }
        return getDate_value().compareTo(v.getDate_value());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getDate_value() {
        return date_value;
    }

    public void setDate_value(Date date_value) {
        this.date_value = date_value;
    }

    public void setDate_value(String date_value) {
        if (date_value.equals("")){
            this.date_value = new Date();
        }
    }

    public Capteur getCapteur() {
        return capteur;
    }

    public void setCapteur(Capteur capteur) {
        this.capteur = capteur;
    }
}
