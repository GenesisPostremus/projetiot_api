package fr.epsi.b3.api.modele;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Entity(name = "capteurs")
@Table(name = "capteurs")
public class Capteur implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    @JsonIgnore
    private Long id;

    @NotNull(message = "entrez un nom")
    @Size(min = 1, message = "entrez un nom")
    private String name;

    @NotNull(message = "entrez une unité")
    @Size(min = 1, message = "entrez une unité")
    private String unite;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "device_id")
    private Device device;

    @JsonManagedReference
    @OneToMany(mappedBy = "capteur", fetch = FetchType.EAGER)
    private List<Valeur> valeurs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public List<Valeur> getValeurs() {
        Collections.sort(valeurs);
        return valeurs;
    }

    public void setValeurs(List<Valeur> valeurs) {
        this.valeurs = valeurs;
    }
}
