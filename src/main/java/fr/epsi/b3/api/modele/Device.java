package fr.epsi.b3.api.modele;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@Entity(name = "devices")
@Table(name = "devices")
public class Device implements Serializable {

    @Id
    @Column(name = "adresse_mac", unique = true, nullable = false)
    private String adresse_mac;

    @JsonManagedReference
    @OneToMany(mappedBy = "device", fetch = FetchType.EAGER)
    private Set<Capteur> capteurs;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JsonBackReference
    @JoinTable(name = "device_user", joinColumns = @JoinColumn(name = "device_id", referencedColumnName = "adresse_mac"),
    inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private Set<Utilisateur> utilisateurs;

    public Device() {
    }

    public Set<Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

    public void setUtilisateurs(Set<Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

    public Set<Capteur> getCapteurs() {
        return capteurs;
    }

    public void setCapteurs(Set<Capteur> capteurs) {
        this.capteurs = capteurs;
    }

    public String getAdresse_mac() {
        return adresse_mac;
    }

    public void setAdresse_mac(String adresse_mac) {
        this.adresse_mac = adresse_mac;
    }

    public Capteur findCapteurInDevice(String typeCapteur){
        for (Capteur c:this.getCapteurs()){
            if (c.getName().equals(typeCapteur)){
                return c;
            }
        }
        return null;
    }
}
