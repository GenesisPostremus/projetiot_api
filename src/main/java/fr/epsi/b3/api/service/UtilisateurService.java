package fr.epsi.b3.api.service;

import fr.epsi.b3.api.dao.UtilisateurDao;
import fr.epsi.b3.api.modele.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UtilisateurService {

    @Autowired
    private UtilisateurDao utilisateurDao = new UtilisateurDao();

    @Transactional
    public void insertUtilisateur(Utilisateur utilisateur) throws UserException {
        utilisateurDao.insertUtilisateur(utilisateur);
    }

    @Transactional
    public Utilisateur getUtilisateurByEmailAndPassword(Utilisateur utilisateur) throws UserException {
        return utilisateurDao.getUtilisateurByEmailAndPassword(utilisateur);
    }

}

