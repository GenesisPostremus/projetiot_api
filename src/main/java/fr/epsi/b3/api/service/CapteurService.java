package fr.epsi.b3.api.service;

import fr.epsi.b3.api.dao.CapteurDao;
import fr.epsi.b3.api.dao.ValeurException;
import fr.epsi.b3.api.modele.Capteur;
import fr.epsi.b3.api.modele.Valeur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CapteurService {

    @Autowired
    private CapteurDao capteurDao;

    @Autowired
    private ValeurService valeurService;

    @Transactional
    public void insertCapteur(Capteur capteur) throws CapteurException, ValeurException {
        List<Valeur> valeurs = capteur.getValeurs();
        capteur.setValeurs(null);
        Capteur insertedCapteur = capteurDao.insertCapteur(capteur);
        if (!valeurs.isEmpty()) {
            for (Valeur v : valeurs) {
                v.setCapteur(insertedCapteur);
                valeurService.insertValeur(v);
            }
        }
    }

    @Transactional
    public Capteur findCapteurById(String s) throws CapteurException {
        return capteurDao.findCapteurById(s);
    }

    @Transactional
    public void updateCapteur(Capteur capteur) {
        capteurDao.updateCapteur(capteur);
    }
}
