package fr.epsi.b3.api.service;

import fr.epsi.b3.api.dao.DeviceDao;
import fr.epsi.b3.api.dao.ValeurException;
import fr.epsi.b3.api.modele.Capteur;
import fr.epsi.b3.api.modele.Device;
import fr.epsi.b3.api.modele.Valeur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Service
public class DeviceService {

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private CapteurService capteurService;

    @Autowired
    private ValeurService valeurService;

    @Transactional
    public List<Device> getAllDevices() {
        return deviceDao.getAllDevices();
    }

    @Transactional
    public void insertDevice(Device device) throws DeviceException, CapteurException, ValeurException {
        Set<Capteur> capteurs = device.getCapteurs();
        device.setCapteurs(null);
        Device insertedDevice = deviceDao.insertDevice(device);
        if (!capteurs.isEmpty()) {
            for (Capteur c : capteurs
            ) {
                c.setDevice(insertedDevice);
                capteurService.insertCapteur(c);
            }
        }
    }

    @Transactional
    public Device findDeviceByMacAdresse(String s) throws DeviceException {
        return deviceDao.findDeviceByMacAdresse(s);
    }

    @Transactional
    public Device findDeviceByMacAdresse(Device device) throws DeviceException {
        return this.findDeviceByMacAdresse(device.getAdresse_mac());
    }

    @Transactional
    public void addValeurToCapteurAndUpdateDevice(Device device, String type_capteur, Valeur valeur) throws CapteurException, ValeurException {
        Capteur capteur = device.findCapteurInDevice(type_capteur);
        if ( capteur != null){
            List<Valeur> valeurs = capteur.getValeurs();
            valeurs.add(valeur);
            capteur.setValeurs(valeurs);
            valeur.setCapteur(capteur);
            valeurService.insertValeur(valeur);
            capteurService.updateCapteur(capteur);
            this.updateDevice(device);
        }
        else {
            throw new CapteurException("Le capteur " + type_capteur + " n'existe pas pour l'appareil " + device.getAdresse_mac() + ".");
        }
    }

    @Transactional
    public void updateDevice(Device device) {
        deviceDao.updateDevice(device);
    }
}
