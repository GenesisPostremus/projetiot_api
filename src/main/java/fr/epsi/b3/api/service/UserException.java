package fr.epsi.b3.api.service;

public class UserException extends Exception {
    public UserException(String message) {
        super(message);
    }
}
