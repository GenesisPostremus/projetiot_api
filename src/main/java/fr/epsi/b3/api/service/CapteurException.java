package fr.epsi.b3.api.service;

public class CapteurException extends Exception {
    public CapteurException(String message) {
        super(message);
    }
}
