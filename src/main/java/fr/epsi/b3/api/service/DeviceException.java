package fr.epsi.b3.api.service;

public class DeviceException extends Exception {
    public DeviceException(String message) {
        super(message);
    }
}
