package fr.epsi.b3.api.service;

import fr.epsi.b3.api.dao.ValeurDao;
import fr.epsi.b3.api.dao.ValeurException;
import fr.epsi.b3.api.modele.Valeur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ValeurService {

    @Autowired
    private ValeurDao valeurDao;

    @Transactional
    public void insertValeur(Valeur valeur) throws ValeurException {
        valeurDao.insertValeur(valeur);
    }
}
