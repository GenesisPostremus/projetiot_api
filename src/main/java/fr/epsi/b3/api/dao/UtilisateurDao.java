package fr.epsi.b3.api.dao;

import fr.epsi.b3.api.modele.Utilisateur;
import fr.epsi.b3.api.service.UserException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Repository
public class UtilisateurDao {

    @PersistenceContext
    private EntityManager entityManager;


    public Utilisateur getUtilisateur(Utilisateur utilisateur) {
        try {
            return entityManager.createQuery("select u from users u where u.user_email = :email or u.user_name = :user_name", Utilisateur.class)
                    .setParameter("email", utilisateur.getUser_email())
                    .setParameter("user_name", utilisateur.getUser_name())
                    .getSingleResult();
        } catch (NoResultException e) {
            throw new NoResultException("Aucun utilisateur trouver pour cette adresse mail ou ce pseudo");
        }
    }

    public Utilisateur getUtilisateurByEmailAndPassword(Utilisateur utilisateur) throws UserException {
        try {
            return entityManager.createQuery("select u from users u where (u.user_email = :email or u.user_name = :name) and u.password = :pass", Utilisateur.class)
                    .setParameter("email", utilisateur.getUser_email())
                    .setParameter("name", utilisateur.getUser_name())
                    .setParameter("pass", utilisateur.getPassword())
                    .getSingleResult();
        } catch (Exception e) {
            throw new UserException("Identifiant ou mot de passe incorrect");
        }
    }

    public void insertUtilisateur(Utilisateur utilisateur) throws UserException {
        try {
            Utilisateur testUtilisateur = getUtilisateur(utilisateur);
            if (testUtilisateur.getUser_email().equals(utilisateur.getUser_email()) || testUtilisateur.getUser_name().equals(utilisateur.getUser_name())) {
                throw new UserException("Email ou pseudo déjà utilisé.");
            }
        } catch (NoResultException e) {
            entityManager.persist(utilisateur);
            entityManager.flush();
        }
    }
}
