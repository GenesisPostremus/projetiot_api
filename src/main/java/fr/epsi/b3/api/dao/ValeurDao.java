package fr.epsi.b3.api.dao;

import fr.epsi.b3.api.modele.Valeur;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;

@Repository
public class ValeurDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void insertValeur(Valeur valeur) throws ValeurException {
        try {
            Timestamp timestamp = new Timestamp(valeur.getDate_value().getTime());
            Valeur valeurExist = entityManager.createQuery("select v from valeurs v where v.date_value = :date and v.value = :value", Valeur.class)
                    .setParameter("date", timestamp)
                    .setParameter("value", valeur.getValue())
                    .getSingleResult();
            if (valeurExist != null) {
                throw new ValeurException("Cette valeur existe déjà.");
            }
        } catch (NoResultException e) {
            entityManager.persist(valeur);
        }
    }

}
