package fr.epsi.b3.api.dao;

import fr.epsi.b3.api.modele.Device;
import fr.epsi.b3.api.service.DeviceException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class DeviceDao {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Device> getAllDevices() {
        return entityManager.createQuery("select d from devices d", Device.class).getResultList();
    }

    public Device insertDevice(Device device) throws DeviceException {
        try {
            if (entityManager.find(Device.class, device.getAdresse_mac()).getAdresse_mac().equals(device.getAdresse_mac())) {
                throw new DeviceException("Cet appareil existe déjà.");
            }
        } catch (NoResultException | NullPointerException e) {
            entityManager.persist(device);
        }
        return device;
    }

    public Device findDeviceByMacAdresse(String adresseMac) throws DeviceException {
        try {
            return entityManager.createQuery("select d from devices d where d.adresse_mac = :adresse", Device.class)
                    .setParameter("adresse", adresseMac)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DeviceException("Aucun appareil correspondant à cette adresse MAC n'a été trouvé");
        }
    }

    public void updateDevice(Device device) {
        entityManager.merge(device);
        entityManager.flush();
    }
}
