package fr.epsi.b3.api.dao;

public class ValeurException extends Exception {
    public ValeurException(String s) {
        super(s);
    }
}
