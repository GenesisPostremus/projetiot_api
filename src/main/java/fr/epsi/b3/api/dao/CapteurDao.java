package fr.epsi.b3.api.dao;

import fr.epsi.b3.api.modele.Capteur;
import fr.epsi.b3.api.service.CapteurException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Repository
public class CapteurDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Capteur insertCapteur(Capteur capteur) throws CapteurException {
        try {
            Capteur capteurExiste = entityManager.createQuery("select c from capteurs c where c.name = :name and c.unite = :unite", Capteur.class)
                    .setParameter("name", capteur.getName())
                    .setParameter("unite", capteur.getUnite())
                    .getSingleResult();
            if (capteurExiste != null) {
                throw new CapteurException("Ce capteur existe déjà");
            }
        } catch (NoResultException e) {
            entityManager.persist(capteur);
            entityManager.flush();
        }
        return capteur;
    }

    public Capteur findCapteurById(String id) throws CapteurException {
        try {
            return entityManager.find(Capteur.class, Long.getLong(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new CapteurException("Aucun capteur n'a était trouvé pour cet ID");
        }
    }

    public void updateCapteur(Capteur capteur) {
        entityManager.merge(capteur);
        entityManager.flush();
    }
}
